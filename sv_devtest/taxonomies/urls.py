from django.urls import path, include
from rest_framework import routers

from . import views, api

router = routers.DefaultRouter()
router.register(r'taxonomy', api.TaxonomyViewSet)
router.register(r'blogtaxonomy', api.BlogTaxonomyViewSet)

urlpatterns = (
    path('api/v1/', include(router.urls)),
    path('list/', views.TaxonomyListView.as_view(), name='taxonomy_list'),
    path('create/', views.TaxonomyCreateView.as_view(), name='taxonomy_create'),
    path('detail/<int:pk>/', views.TaxonomyDetailView.as_view(), name='taxonomy_detail'),
    path('update/<int:pk>/', views.TaxonomyUpdateView.as_view(), name='taxonomy_update'),
)

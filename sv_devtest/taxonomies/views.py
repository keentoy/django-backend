from django.views.generic import DetailView, ListView, UpdateView, CreateView

from .forms import TaxonomyForm
from .models import Taxonomy


class TaxonomyListView(ListView):
    model = Taxonomy


class TaxonomyCreateView(CreateView):
    model = Taxonomy
    form_class = TaxonomyForm


class TaxonomyDetailView(DetailView):
    model = Taxonomy


class TaxonomyUpdateView(UpdateView):
    model = Taxonomy
    form_class = TaxonomyForm

from django.urls import path, include
from rest_framework import routers

from . import views, api

router = routers.DefaultRouter()
router.register(r'comment', api.CommentViewSet)

urlpatterns = (
    path('api/v1/', include(router.urls)),
    path('list/', views.CommentListView.as_view(), name='comment_list'),
    path('create/', views.CommentCreateView.as_view(), name='comment_create'),
    path('detail/<int:pk>/', views.CommentDetailView.as_view(), name='comment_detail'),
    path('update/<int:pk>/', views.CommentUpdateView.as_view(), name='comment_update'),
)
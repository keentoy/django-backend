# Spring Valley backend developer exam
## Instructions
Fork this repo and create a pull request when done.
## Overview
This is a basic Django 2.1/Python 3.6 setup that the applicant needs to create functionality to. Included in this repo is a `requirements.txt` file that the applicant is expected to use to populate his/her virtual environment. This exam does not assume a development environment other than the one needed to run Django 2.1 and Python 3.6.

### Relationships
The project has 3 apps: blog, comments, and taxonomies. 
- A blog post minimally contains  `title`,  `body`, and `created` `datetime` fields in addition to any relationship fields it may need to have
- A comment minimally contains `author`, `email`, and `comment` fields in addition to any relationship fields it may need to have
- A blog post may have 0 to many comments
- A comment may only be associated with one blog post
- A taxonomy/tag minimally contains a `term` field in addition to any relationship fields it may need to have
- A blog post may have 0 to many taxonomies (or tags)
- A taxonomy (or tag) may be attached to 0 to many blog posts

#### Attributes
- Blog
    1. created - `datetime`
    1. title - `varchar`
    1. body - `text`
- Comment
    1. created - `datetime`
    1. blog - `foreign key`
    1. author - `varchar`
    1. email - `varchar`
    1. comment - `text`
- Taxonomy
    1. created - `datetime`
    1. term - `varchar`
- BlogTaxonomy (Associative Entity)
    1. blog - `foreign key`
    1. taxonomy - `foregin key`

### Expected Outcomes
1. Backend Core - **intern**
    1. Applicant must be familiar enough with source control to be able to fork this repo, and send a pull request at the end of the process
    1. Applicant must be able to properly configure own development environment, using the provided `requirements.txt` file.
    1. Applicant must be able to run the minimal test suite via `manage.py`
    1. Applicant must be able to build models (`models.py`) for each app that will allow the required functionality
    1. Debug and remove/comment out if necessary all other extraneous/erroneous code
    1. **Extra Credit**
        - Create views that will allow the end user to add taxonomy to a blog post
        - Create views that will allow visitors to post a comment
        - Create views to update and delete blog posts, along with their associated comments
1. Backend Plus - **employment**
    1. Applicant must make the information stored in the database available via a GET request at an API endpoint
    1. The API should be made using Django Rest Framework
    1. GET requesting a blog object should:
        - Present a JSON representation of the blog post, and all associated objects (comments and taxonomies) as nested attributes
    1. POST requesting a blog object should:
        - Create a new blog post
    1. **Extra Credit**
        - GET request to a taxonomy object that will return associated blog posts
        - POST request that will add taxonomy and comment objects to associated blog posts
        - Implement a GraphQL API for queries and mutations